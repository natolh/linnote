#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""
Miscellaneaous tools for the project.

Author: Anatole Hanniet, 2016-2018.
License: Mozilla Public License, see 'LICENSE.txt' for details.
"""

from .database import configure, SESSION, WEBSESSION
