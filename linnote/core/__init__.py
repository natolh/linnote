#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""
Heart of the project

Please operate with vigilance, blood transfusion not available.

Author: Anatole Hanniet, 2016-2018.
License: Mozilla Public License, see 'LICENSE.txt' for details.
"""
