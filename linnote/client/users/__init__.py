# -*- coding: utf-8 -*-

u"""
Users application module.

Author: Anatole Hanniet, 2016-2018.
License: Mozilla Public License, see 'LICENSE.txt' for details.
"""

from .routes import ROUTES as BLUEPRINT
